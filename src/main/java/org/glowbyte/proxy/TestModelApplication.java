package org.glowbyte.proxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;


@SpringBootApplication
public class TestModelApplication {

	public static void main(String[] args)   {
	    SpringApplication.run(TestModelApplication.class, args);

	}



    @RestController
    @RequestMapping("${addresses.base}")
    public class mainController {

	    Gson gson = new GsonBuilder().create();

        @PostMapping("${addresses.model}")
        private String modelExecution(@RequestBody String body,
                                      @Value("${multiplier}") Double multiplier){
            System.out.println(body);
            ArrayList<Object> params = gson.fromJson(body, ArrayList.class);
            Double y = 0d;
            for (Object param : params) {
                y += ((Double) param) * multiplier;
            }
            HashMap y_res = new HashMap();
            y_res.put("result", y);

            return gson.toJson(y_res);
        }
    }


}
